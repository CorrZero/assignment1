In order to build, install, and use my project you must first have Visual Studio 2015, 2017. Then download PCampbellAssignment1.zip from the download repository.
Once you have the visual studio program run it and let the program start up.
Unzip my project from the zip file then open the .sln or solution from the Visual Studio program.
Wait a bit for Visual Studio to build and open my project, then once it's done loading hit start to run or use the project.

LICENSE
Copyright (c) Peter Campbell 2019.

I licensed with my name because I have programmed this assignment myself.